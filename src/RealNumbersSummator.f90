module RealNumbers_Summator

    interface NeumaierSum
    
        subroutine NeumaierSum4(input, sum)
            real (kind = 4), intent(in)  :: input(:)
            real (kind = 4), intent(out) :: sum
        end subroutine
        
        subroutine NeumaierSum8(input, sum)
            real (kind = 8), intent(in)  :: input(:)
            real (kind = 8), intent(out) :: sum
        end subroutine
        
        subroutine NeumaierSum16(input, sum)
            real (kind = 16), intent(in) :: input(:)
            real (kind = 16), intent(out) :: sum
        end subroutine
    
    end interface
    
contains
endmodule

subroutine NeumaierSum4(input, sum)
    
    implicit none
    
    real    (kind = 4), intent(in)  :: input(:)
    real    (kind = 4), intent(out) :: sum
    
    real    (kind = 4) :: c, t
    integer (kind = 4) :: n, i
    
    n = size(input)
    c = 0.0d0
    sum = input(1)
    
    do i = 2, n
        t = sum + input(i)
        
        if (abs(sum) .GE. abs(input(i))) then
            c = c + (sum - t) + input(i)
        else
            c = c + (input(i) - t) + sum
        end if
        
        sum = t
        
    end do
    
    sum = sum + c

end subroutine

subroutine NeumaierSum8(input, sum)
    
    implicit none
    
    real    (kind = 8), intent(in)  :: input(:)
    real    (kind = 8), intent(out) :: sum
    
    real    (kind = 8) :: c, t
    integer (kind = 4) :: n, i
    
    n = size(input)
    c = 0.0d0
    sum = input(1)
    
    do i = 2, n
        t = sum + input(i)
        
        if (abs(sum) .GE. abs(input(i))) then
            c = c + (sum - t) + input(i)
        else
            c = c + (input(i) - t) + sum
        end if
        
        sum = t
        
    end do
    
    sum = sum + c

end subroutine

subroutine NeumaierSum16(input, sum)
    implicit none
    
    real    (kind = 16), intent(in)  :: input(:)
    real    (kind = 16), intent(out) :: sum
    
    real    (kind = 16) :: c, t
    integer (kind = 4)  :: n, i
    
    n = size(input)
    c = 0.0d0
    sum = input(1)
    
    do i = 2, n
        t = sum + input(i)
        
        if (abs(sum) .GE. abs(input(i))) then
            c = c + (sum - t) + input(i)
        else
            c = c + (input(i) - t) + sum
        end if
        
        sum = t
        
    end do
    
    sum = sum + c

end subroutine