program Project01

    use Heat_FDM_Solver
    use AvgError_Calculator
    use Args_Parser
    
    implicit none
    
    integer (kind = 4) :: parsing_status
    
    integer (kind = 4), parameter :: MAX_N = 10000
    integer (kind = 4)  :: N, i
    
    real    (kind = 4),  allocatable :: solution_vector_k4(:)
    real    (kind = 8),  allocatable :: solution_vector_k8(:)
    real    (kind = 16), allocatable :: solution_vector_k16(:)
    
    real    (kind = 4)  :: h4, avgError4
    real    (kind = 8)  :: h8, avgError8
    real    (kind = 16) :: h16, avgError16
    
    real    (kind = 4),  allocatable :: exact_values_vector_k4(:)
    real    (kind = 8),  allocatable :: exact_values_vector_k8(:)
    real    (kind = 16), allocatable :: exact_values_vector_k16(:)
    
    parsing_status = parse_arguments()
    
    if (parsing_status .EQ. PARSE_OK) then
    
        select case(kind_type)
            case('s')
                write(*, *) 'Determinating average error of FDM for single precision'
                
                do N = 1, MAX_N
                
                    ! ----- solve equation K = 4 -------
                    allocate(solution_vector_k4(0:N))
                    call solve(N, solution_vector_k4)
                    
                    ! ----- count error ----------------
                    
                    h4 = real(1, kind = 4) / N
                    allocate(exact_values_vector_k4(0:N))
                    do i = 0, N
                        exact_values_vector_k4(i) = h4 * i 
                    end do
                    
                    call calculate_AvgError(solution_vector_k4, exact_values_vector_k4, avgError4)
                    
                    ! ----- print result ---------------
                    write(*, *) 'N:', N, 'Error: ', avgError4
                    
                    ! ----- clean for next loop --------
                    if (allocated(solution_vector_k4)) deallocate(solution_vector_k4)
                    if (allocated(exact_values_vector_k4)) deallocate(exact_values_vector_k4)
                    
                end do
                
                
            case('d')
                write(*, *) 'Determinating average error of FDM for double precision'
                
                do N = 1, MAX_N
                
                    ! ----- solve equation K = 8 -------
                    allocate(solution_vector_k8(0:N))
                    call solve(N, solution_vector_k8)
                    
                    ! ----- count error ----------------
                    
                    h8 = real(1, kind = 8) / N
                    allocate(exact_values_vector_k8(0:N))
                    do i = 0, N
                        exact_values_vector_k8(i) = h8 * i 
                    end do
                    
                    call calculate_AvgError(solution_vector_k8, exact_values_vector_k8, avgError8)
                    
                    ! ----- print result ---------------
                    write(*, *) 'N:', N, 'Error: ', avgError8
                    
                    ! ----- clean for next loop --------
                    if (allocated(solution_vector_k8)) deallocate(solution_vector_k8)
                    if (allocated(exact_values_vector_k8)) deallocate(exact_values_vector_k8)
                    
                end do
                
            case('q')
                write(*, *) 'Determinating average error of FDM for quadruple precision'
                
                do N = 1, MAX_N
                
                    ! ----- solve equation K = 16 -------
                    allocate(solution_vector_k16(0:N))
                    call solve(N, solution_vector_k16)
                    
                    ! ----- count error ----------------
                    
                    h16 = real(1, kind = 16) / N
                    allocate(exact_values_vector_k16(0:N))
                    do i = 0, N
                        exact_values_vector_k16(i) = h16 * i 
                    end do
                    
                    call calculate_AvgError(exact_values_vector_k16, solution_vector_k16, avgError16)
                    
                    ! ----- print result ---------------
                    write(*, *) 'N:', N, 'Error: ', avgError16
                    
                    ! ----- clean for next loop --------
                    if (allocated(solution_vector_k16)) deallocate(solution_vector_k16)
                    if (allocated(exact_values_vector_k16)) deallocate(exact_values_vector_k16)
                    
                end do

        end select      

    end if
    
end program Project01