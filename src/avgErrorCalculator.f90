module AvgError_Calculator

    interface calculate_AvgError
    
        subroutine calculate_AvgError4(approx_values, exact_values, avg_error)
            real    (kind = 4), intent(in)  :: approx_values(:)
            real    (kind = 4), intent(in)  :: exact_values(:)
            real    (kind = 4), intent(out) :: avg_error
        end subroutine
        
        subroutine calculate_AvgError8(approx_values, exact_values, avg_error)
            real    (kind = 8), intent(in)  :: approx_values(:)
            real    (kind = 8), intent(in)  :: exact_values(:)
            real    (kind = 8), intent(out) :: avg_error
        end subroutine
        
        subroutine calculate_AvgError16(approx_values, exact_values, avg_error)
            real    (kind = 16), intent(in)  :: approx_values(:)
            real    (kind = 16), intent(in)  :: exact_values(:)
            real    (kind = 16), intent(out) :: avg_error
        end subroutine
        
    end interface

contains
end module

subroutine calculate_AvgError4(approx_values, exact_values, avg_error) 
    use RealNumbers_Summator
    
    implicit none
    
    real    (kind = 4), intent(in)  :: approx_values(:)
    real    (kind = 4), intent(in)  :: exact_values(:)
    real    (kind = 4), intent(out) :: avg_error
    
    integer (kind = 4) :: n
    real    (kind = 4) :: tmp_sum
    real    (kind = 4), allocatable :: abs_values(:)
    
    n = size(exact_values)
    allocate(abs_values(n))
    
    abs_values = abs(exact_values - approx_values)
    call NeumaierSum(abs_values, tmp_sum)

    avg_error = tmp_sum / n
    
    if (allocated(abs_values)) deallocate(abs_values)
    
end subroutine

subroutine calculate_AvgError8(approx_values, exact_values, avg_error) 
    use RealNumbers_Summator
    
    implicit none
    
    real    (kind = 8), intent(in)  :: approx_values(:)
    real    (kind = 8), intent(in)  :: exact_values(:)
    real    (kind = 8), intent(out) :: avg_error
    
    integer (kind = 4) :: n
    real    (kind = 8) :: tmp_sum
    real    (kind = 8), allocatable :: abs_values(:)
    
    n = size(exact_values)
    
    allocate(abs_values(n))
    
    abs_values = abs(exact_values - approx_values)
    call NeumaierSum(abs_values, tmp_sum)

    avg_error = tmp_sum / n
    
    if (allocated(abs_values)) deallocate(abs_values)
    
end subroutine

subroutine calculate_AvgError16(approx_values, exact_values, avg_error) 
    use RealNumbers_Summator
    
    implicit none
    
    real    (kind = 16), intent(in)  :: approx_values(:)
    real    (kind = 16), intent(in)  :: exact_values(:)
    real    (kind = 16), intent(out) :: avg_error
    
    integer (kind = 4) :: n
    real    (kind = 16) :: tmp_sum
    real    (kind = 16), allocatable :: abs_values(:)
    
    n = size(exact_values)
    
    allocate(abs_values(n))
    
    abs_values = abs(exact_values - approx_values)
    call NeumaierSum(abs_values, tmp_sum)

    avg_error = tmp_sum / n
    
    if (allocated(abs_values)) deallocate(abs_values)
    
end subroutine
