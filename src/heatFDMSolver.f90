module Heat_FDM_Solver

    interface solve
    
        subroutine solve4(n, solution_vector)
            integer (kind = 4), intent(in)  :: n
            real    (kind = 4), intent(out) :: solution_vector(0:n)
        end subroutine
        
        subroutine solve8(n, solution_vector)
            integer (kind = 4), intent(in)  :: n
            real    (kind = 8), intent(out) :: solution_vector(0:n)
        end subroutine
        
        subroutine solve16(n, solution_vector)
            integer (kind = 4), intent(in)  :: n
            real    (kind = 16), intent(out) :: solution_vector(0:n)
        end subroutine
        
    end interface
    
contains  
end module

subroutine solve4(n, solution_vector)
    implicit none
    
    integer (kind = 4), intent(in)  :: n
    real    (kind = 4), intent(out) :: solution_vector(0:n)
    
    integer (kind = 4) :: i
    real    (kind = 4) :: h, a, b, c
    real    (kind = 4), allocatable :: packed_matrix(:, :)
    real    (kind = 4), allocatable :: rightSide_vector(:)
    real    (kind = 4) :: factor
    
    real    (kind = 4), parameter :: u0 = 0
    real    (kind = 4), parameter :: u1 = 1
    real    (kind = 4), parameter :: f = 0
    
    if (n .GT. 1) then
    
        ! calculate coefficients
        h = real(1, kind = 4) / n
        a = 1 / (h ** 2)
        b = -2 / (h ** 2)
        c = a
        
        allocate(packed_matrix(1:(n - 1), 3))
        allocate(rightSide_vector(1:(n - 1)))
        
        ! fill packed matrix of coefficients        
        packed_matrix = 0
          
        packed_matrix(1, 2:3) = (/b, a/)
        
        do i = 2, (n - 2) 
            packed_matrix(i, :) = (/c, b, a/)
        end do
        
        packed_matrix(n - 1, 1:2) = (/c, b/)
        
        ! fill right side vector
        rightSide_vector = f
        rightSide_vector(1)     = f - u0 * a
        rightSide_vector(n - 1) = f - u1 * c
        
        ! Gaussian elimination for matrix (n - 1) x 3 and right side vector
        rightSide_vector(1) = rightSide_vector(1) / packed_matrix(1, 2)
        packed_matrix(1, :) = packed_matrix(1, :) / packed_matrix(1, 2)
        
        do i = 2, (n-1)
            factor = packed_matrix(i, 1) / packed_matrix(i - 1, 2)
            packed_matrix(i, 1) = 0
            packed_matrix(i, 2) = packed_matrix(i, 2) - factor * packed_matrix(i - 1, 3)
            rightSide_vector(i) = rightSide_vector(i) - factor * rightSide_vector(i - 1)
            
            rightSide_vector(i) = rightSide_vector(i) / packed_matrix(i, 2)
            packed_matrix(i, :) = packed_matrix(i, :) / packed_matrix(i, 2)
        end do

        ! backward substitute after Gaussian elimination
        solution_vector(n - 1) = rightSide_vector(n - 1);
        do i = (n - 2), 1, -1
          solution_vector(i) = rightSide_vector(i) - packed_matrix(i, 3) * solution_vector(i + 1);
        end do
        
    end if
    
    ! add boundary conditions to solution
    solution_vector(0) = u0
    solution_vector(n) = u1
    
    if (allocated(packed_matrix))    deallocate(packed_matrix)
    if (allocated(rightSide_vector)) deallocate(rightSide_vector)
    
end subroutine

subroutine solve8(n, solution_vector)
    implicit none
    
    integer (kind = 4), intent(in)  :: n
    real    (kind = 8), intent(out) :: solution_vector(0:n)
    
    integer (kind = 4) :: i
    real    (kind = 8) :: h, a, b, c
    real    (kind = 8), allocatable :: packed_matrix(:, :)
    real    (kind = 8), allocatable :: rightSide_vector(:)
    real    (kind = 8) :: factor
    
    real    (kind = 8), parameter :: u0 = 0
    real    (kind = 8), parameter :: u1 = 1
    real    (kind = 8), parameter :: f = 0
    
    if (n .GT. 1) then
    
        ! calculate coefficients
        h = real(1, kind = 8) / n
        a = 1 / (h ** 2)
        b = -2 / (h ** 2)
        c = a
        
        allocate(packed_matrix(1:(n - 1), 3))
        allocate(rightSide_vector(1:(n - 1)))
        
        ! fill packed matrix of coefficients        
        packed_matrix = 0
          
        packed_matrix(1, 2:3) = (/b, a/)
        
        do i = 2, (n - 2) 
            packed_matrix(i, :) = (/c, b, a/)
        end do
        
        packed_matrix(n - 1, 1:2) = (/c, b/)
        
        ! fill right side vector
        rightSide_vector = f
        rightSide_vector(1)     = f - u0 * a
        rightSide_vector(n - 1) = f - u1 * c
        
        ! Gaussian elimination for matrix (n - 1) x 3 and right side vector
        rightSide_vector(1) = rightSide_vector(1) / packed_matrix(1, 2)
        packed_matrix(1, :) = packed_matrix(1, :) / packed_matrix(1, 2)
        
        do i = 2, (n-1)
            factor = packed_matrix(i, 1) / packed_matrix(i - 1, 2)
            packed_matrix(i, 1) = 0
            packed_matrix(i, 2) = packed_matrix(i, 2) - factor * packed_matrix(i - 1, 3)
            rightSide_vector(i) = rightSide_vector(i) - factor * rightSide_vector(i - 1)
            
            rightSide_vector(i) = rightSide_vector(i) / packed_matrix(i, 2)
            packed_matrix(i, :) = packed_matrix(i, :) / packed_matrix(i, 2)
        end do

        ! backward substitute after Gaussian elimination
        solution_vector(n - 1) = rightSide_vector(n - 1);
        do i = (n - 2), 1, -1
          solution_vector(i) = rightSide_vector(i) - packed_matrix(i, 3) * solution_vector(i + 1);
        end do
        
    end if
    ! add boundary conditions to solution
    solution_vector(0) = u0
    solution_vector(n) = u1
    
    if (allocated(packed_matrix))    deallocate(packed_matrix)
    if (allocated(rightSide_vector)) deallocate(rightSide_vector)
    
end subroutine

subroutine solve16(n, solution_vector)
    implicit none
    
    integer (kind = 4), intent(in)  :: n
    real    (kind = 16), intent(out) :: solution_vector(0:n)
    
    integer (kind = 4) :: i
    real    (kind = 16) :: h, a, b, c
    real    (kind = 16), allocatable :: packed_matrix(:, :)
    real    (kind = 16), allocatable :: rightSide_vector(:)
    real    (kind = 16) :: factor
    
    real    (kind = 16), parameter :: u0 = 0
    real    (kind = 16), parameter :: u1 = 1
    real    (kind = 16), parameter :: f = 0
    
    if (n .GT. 1) then
        ! calculate coefficients
        h = real(1, kind = 16) / n
        a = 1 / (h ** 2)
        b = -2 / (h ** 2)
        c = a
        
        allocate(packed_matrix(1:(n - 1), 3))
        allocate(rightSide_vector(1:(n - 1)))
        
        ! fill packed matrix of coefficients        
        packed_matrix = 0
          
        packed_matrix(1, 2:3) = (/b, a/)
        
        do i = 2, (n - 2) 
            packed_matrix(i, :) = (/c, b, a/)
        end do
        
        packed_matrix(n - 1, 1:2) = (/c, b/)
        
        ! fill right side vector
        rightSide_vector = f
        rightSide_vector(1)     = f - u0 * a
        rightSide_vector(n - 1) = f - u1 * c
        
        ! Gaussian elimination for matrix (n - 1) x 3 and right side vector
        rightSide_vector(1) = rightSide_vector(1) / packed_matrix(1, 2)
        packed_matrix(1, :) = packed_matrix(1, :) / packed_matrix(1, 2)
        
        do i = 2, (n-1)
            factor = packed_matrix(i, 1) / packed_matrix(i - 1, 2)
            packed_matrix(i, 1) = 0
            packed_matrix(i, 2) = packed_matrix(i, 2) - factor * packed_matrix(i - 1, 3)
            rightSide_vector(i) = rightSide_vector(i) - factor * rightSide_vector(i - 1)
            
            rightSide_vector(i) = rightSide_vector(i) / packed_matrix(i, 2)
            packed_matrix(i, :) = packed_matrix(i, :) / packed_matrix(i, 2)
        end do

        ! backward substitute after Gaussian elimination
        solution_vector(n - 1) = rightSide_vector(n - 1);
        do i = (n - 2), 1, -1
          solution_vector(i) = rightSide_vector(i) - packed_matrix(i, 3) * solution_vector(i + 1);
        end do
        
    end if
    
    ! add boundary conditions to solution
    solution_vector(0) = u0
    solution_vector(n) = u1
    
    if (allocated(packed_matrix))    deallocate(packed_matrix)
    if (allocated(rightSide_vector)) deallocate(rightSide_vector)
    
end subroutine