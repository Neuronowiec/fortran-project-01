module Args_Parser

    implicit none

    character(1), protected :: kind_type
    
    integer (kind = 4), parameter :: PARSE_OK = 1
    integer (kind = 4), parameter :: PARSE_ERR = 0
    
contains

    function parse_arguments() result (parsing_status)
        use iso_fortran_env, only: ERROR_UNIT ! access computing environment
    
        implicit none
        
        integer (kind = 4) :: parsing_status
        integer (kind = 4) :: args_count, length, status
        character(100)     :: arg_value
        
        ! if args_count != 1 -> PARSE_ERR
        args_count = command_argument_count()
        if (args_count .NE. 1) then
            write(ERROR_UNIT, *) 'Wrong number of arguments; should be: project01 s|d|q'
            
            parsing_status = PARSE_ERR
            return
        end if

        ! if length != 1 -> PARSE_ERR
        call get_command_argument(1, arg_value, length, status)
        if (length .NE. 1) then
            write(ERROR_UNIT, *) 'Wrong argument; should be: project01 s|d|q'
            
            parsing_status = PARSE_ERR
            return
        end if
        
        read(arg_value, *) kind_type

        ! if kind_type != s|d|q -> PARSE_ERR
        if (.NOT. ((kind_type .EQ. 's') .OR. (kind_type .EQ. 'd') .OR. (kind_type .EQ. 'q'))) then
            write(ERROR_UNIT, *) 'Wrong argument; should be: project01 s|d|q'
            
            parsing_status = PARSE_ERR
            return
        end if
        
        parsing_status = PARSE_OK
    
    end function
    
end module