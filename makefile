# dir names
DSRC = src
DMODULE = mod
DBUILD = build
DRES = res

PROGN = project01

FC = gfortran
FFLAGS = -std=f2008 -fimplicit-none

$(PROGN): all

all: $(DSRC)/main.f90 $(DSRC)/argsParser.f90 $(DSRC)/realNumbersSummator.f90 $(DSRC)/heatFDMSolver.f90 $(DSRC)/avgErrorCalculator.f90
	mkdir -p $(DMODULE)
	mkdir -p $(DBUILD)
	$(FC) $(FFLAGS) -I$(DMODULE) -J$(DMODULE) -o$(DBUILD)/$(PROGN) $(DSRC)/argsParser.f90 $(DSRC)/realNumbersSummator.f90 $(DSRC)/heatFDMSolver.f90 $(DSRC)/avgErrorCalculator.f90 $(DSRC)/main.f90
	
task01: $(DBUILD)/$(PROGN)
	mkdir -p $(DRES)
	$(DBUILD)/$(PROGN) s > $(DRES)/res_single
	$(DBUILD)/$(PROGN) d > $(DRES)/res_double
	$(DBUILD)/$(PROGN) q > $(DRES)/res_quadruple
	
clean:
	rm -r $(DMODULE)
	
.PHONY: clean all