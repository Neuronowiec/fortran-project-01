# Fortran Project no. 1

Project no. 1 at *Programming in Fortran Language* Course. 
Detail information about the task can be found [here](http://home.agh.edu.pl/~macwozni/fort/projekt1.pdf).

## Compilator

**gfortran v. 5.4.0**

## Compiling

To compile the code, type:
```
make project01
```
It will produce `./build/project01`

## Running
Program solves heat equation problem, compares results with standard sollution: *u(x) = x* and counts an average numerical error. It returns errors for all density of splitting grid (*n ∈ [1, 10^4]*)

---

To run the program for one precision, type:
```
./build/project1 PRECISION
```
where `PRECISION` means precision of floating-point numbers used during calculations:

* `s` - single precision (kind = 4)
* `d` - double precision (kind = 8)
* `q` - quadrable precision (kind = 16)

---

To run programs for all 3 precisions at once, type:

```
make task01
```
It will produce results for all 3 precisions, and put them into files:

* `res/res_single`
* `res/res_double`
* `res/res_quadruple`

## Graphs

Graphs illustrating program results can be found in `./res` directory.
